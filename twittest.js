Tweets = new Mongo.Collection("tweets");
UserFavorites = new Mongo.Collection("userfavorites");

if (Meteor.isClient) {
  Template.tweetList.helpers({
    tweets: function() {
      return Tweets.find({}, {sort: {createdAt: -1}});
    }
  });

  Template.tweetItem.helpers({
    isFavorite: function() {
      if (!Meteor.user()) {
        return false;
      }
      return UserFavorites.find({user: Meteor.user().username, tweet: this._id}).count() > 0;
    },

    favoriteCount: function() {
      return UserFavorites.find({tweet: this._id}).count();
    },

    ownTweet: function() {
      if (!Meteor.user()) {
        return false;
      }
      return Meteor.user().username == this.username;
    }
  });

  Template.tweetItem.events({
    'click .favorite': function(event) {
      UserFavorites.insert({
        user: Meteor.user().username,
        tweet: this._id
      });
      event.preventDefault();
    },

    'click .unfavorite': function(event) {
      var userFavorite = UserFavorites.findOne({
        user: Meteor.user().username,
        tweet: this._id
      });
      UserFavorites.remove(userFavorite._id);
      event.preventDefault();
    },

    'click .delete': function(event) {
      Tweets.remove(this._id);
      event.preventDefault();
    }
  });

  Template.body.events({
    'submit .new-tweet': function(event) {
      Tweets.insert({
        username: Meteor.user().username,
        tweet: $('#new-tweet-text').val(),
        createdAt: new Date()
      });

      $('#new-tweet-text').val('');
      event.preventDefault();
    }
  });

  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    if (Tweets.find().count() === 0) {
      Tweets.insert({username: "kurtgerm", tweet: "Hello World!", createdAt: new Date()});
      Tweets.insert({username: "goabroad", tweet: "What was the best meal you ever had?", createdAt: new Date()});
      Tweets.insert({username: "anne", tweet: "I hate Monday exams. :(", createdAt: new Date()});
    }
  });
}